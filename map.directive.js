(function ()
{
    'use strict';

    angular
        .module('isaac-nadar.my-directive')
        .directive('ivnMyDirective', trackTraceMap);

    /** @ngInject */
    function trackTraceMap()
    {
        return {
            restrict: 'A',
            scope   : {
                origin:'@',
                waypoints: '@',
                destination: '@'
            },
            template : `<map zoom="1" id="my-map" center="{{ctrl.parent.origin}}" style="height:110%" >
                            <directions
                                draggable="true"
                                polyline-options='{strokeColor: "red"}'
                                suppress-markers=true
                                panel="directions-panel"
                                travel-mode="{{ctrl.parent.travelMode}}"
                                waypoints={{waypoints}}
                                origin={{origin}}
                                destination={{destination}}>
                            </directions>
                            
                            <marker icon="{{ctrl.parent.origin_map}}"  position="51.546550, 0.026345" ></marker>
                            <marker icon="{{ctrl.parent.customIcon}}" position="{{ctrl.parent.wayPoints[0].location.lat}},{{ctrl.parent.wayPoints[0].location.lng}}"  ></marker>
                            <marker icon="{{ctrl.parent.destination_map}}"  position="51.5493953, 0.0412878" ></marker>
						</map>`
        };
    }
})();
